import React, { Fragment } from "react";
import { CssBaseline } from "@material-ui/core";
import * as Style from "./ProjectTitleStyles";

export default function ProjectTitle(props) {
  return (
    <Fragment>
      <CssBaseline />
      <Style.MyProjectTitle>{props.data}</Style.MyProjectTitle>
    </Fragment>
  );
}
